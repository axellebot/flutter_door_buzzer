import 'dart:async';
import 'dart:io' as io;

import 'package:path/path.dart';
import 'package:flutter_door_buzzer/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE User(id INTEGER PRIMARY KEY, password TEXT, email TEXT, token TEXT)");
    print("Created tables");
  }

  Future<int> saveUser(User user) async {
    // TODO: replace user entirely
    var dbClient = await db;
    var _user = {
      'password': user.password,
      'email': user.user.email,
      'token': user.user.token
    };
    return await dbClient.insert("User", _user);
  }

  Future<int> deleteUsers() async {
    var dbClient = await db;
    return await dbClient.delete("User");
  }

  Future<bool> isLoggedIn() async {
    var dbClient = await db;
    var res = await dbClient.query("User");
    return res.length > 0 ? true : false;
  }

  Future<User> getUser() async {
    var dbClient = await db;
    var res = await dbClient.query("User");
    print(res.length.toString() + " records gefunden: " + res.toString());
    if (res != null && res.length > 0) {
      return User.fromJson({'email': res[0]['email'], 'token': res[0]['token'], 'password': res[0]['password']});
    }
    return null;
  }

}
