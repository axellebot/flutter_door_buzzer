import 'dart:async';
import 'dart:convert';
import 'package:flutter_door_buzzer/models/user.dart';
import 'package:flutter_door_buzzer/utils/network_util.dart';
import 'package:flutter_door_buzzer/config.dart';
import 'package:http/http.dart' as http;

class SignupResponse {
  final User user;
  final String username;

  SignupResponse({this.user, this.username});

  factory SignupResponse.fromJson(Map<String, dynamic> json) {
    return SignupResponse(
        username: json['user']['username']
    );
  }
}

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();

  static final loginURl = CWRKNG_ENDPOINT + "/login";
  static final signupURl = CWRKNG_ENDPOINT + "/signup";
  // static final _apiKey = "xxxyyyzzz";

/*  Future<User> login(String username, String password) {
    return _netUtil.post(loginURl, body: {
      "token": _apiKey,
      "username": username,
      "password": password
    }).then((dynamic res) {
      print(res.toString());
      if (res["error"]) throw new Exception(res["error_msg"]);
      return new User.map(res["user"]);
    });
  }*/

  Future<User> signup(String username, String password, String email) async {
    final payload = jsonEncode({
      'username': username,
      'password': password,
      'email': email
    });

    final headers = {
      'Content-Type': 'application/json'
    };
    print('Sending body: ' + payload.toString());

    final response =
    await http.post(signupURl, body: payload, headers: headers);

    print('Alpha');
    if (response.statusCode == 201) {
      // If the call to the server was successful, parse the JSON
      // return SignupResponse.fromJson(json.decode(response.body));
      print('Signup successful: ' + response.body);
      final _body = jsonDecode(response.body)['user'];
      return new User.map(_body);
    } else {
      print('Failed to signup');
      // If that call was not successful, throw an error.
      throw Exception('Failed to signup');
    }
  }

  Future<User> signupOld(String username, String password, String email) {

    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    return _netUtil.post(signupURl, body: {
      "username": username,
      "password": password,
      "emai": email
    }, headers: requestHeaders
    ).then((dynamic res) {
      print('Got; ' + res.toString());
      if (res["error"]) throw new Exception(res["error_msg"]);
      return new User.map(res["user"]);
    });
  }

}
